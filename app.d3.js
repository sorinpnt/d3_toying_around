var filterValueDomCache = undefined;

var startApp = function() {
    var appData = extractData( mock_data );
    createCanvas( appData.canvas_info );
    renderLevelDelimiters( appData.canvas_info,  appData.graph_data );
    renderData( appData.canvas_info, appData.graph_data );
    createDomCache();
    addClickHandlers(appData.canvas_info, appData.graph_data);
};

var addClickHandlers = function( canvasElement, dataSet ) {
    var textToShow = undefined;
    var displayText = function( key) {
        canvasElement.element
            .selectAll("text.companyInfo")
            .text(function(d) {
                return d[key];
            });
    };
    $("input[name='nodeInfoText']").on("click", function(event){
        textToShow = this.value;
        displayText(textToShow);
    });
 
    $("#filterSection a.filterKey").on("click", function(event){
        event.preventDefault();
        var keyName = $(this).html().toLowerCase();
        var keyValue = filterValueDomCache.val().toLowerCase();
        renderData( canvasElement, dataSet );
        displayText(textToShow);
        var filter = function(d){
            return !(d[keyName].toString().toLowerCase() === keyValue.toString().toLowerCase());
        };
        canvasElement.element
            .selectAll("circle")
                .filter(filter)
                .attr("display", "none")
         canvasElement.element.selectAll("text.companyInfo")
                .filter(filter)
                .attr("display", "none");
    });

    $("#resetFilterBtn").on("click", function(event) {
        event.preventDefault();
        renderData( canvasElement, dataSet );
    });
};

var extractData = function( data ) {
    data.canvas_info.element = undefined;
    data.canvas_info.height_per_level = data.canvas_info.height / data.graph_data.length;
    data.canvas_info.usable_canvas_space = data.canvas_info.width - data.canvas_info.node_radius * 2;
    return data;
};  

var createCanvas = function( canvas ) {
    canvas.element = d3.select("#canvasContainer")
                        .append("svg")
                            .attr("width", canvas.width)
                            .attr("height", canvas.height)
                            .style("padding", canvas.node_radius)
                            .style("background-color", "#d3d3d3");  
};

var renderData = function( canvas, graphData ) {
    graphData.forEach( function( graphLevelData ) { 
        renderLevel (canvas, graphLevelData); 
    });
};

var renderLevelDelimiters = function ( canvas, graphData )  {
    var levelLimits = [];
    graphData.forEach( function(levelData) {
        levelLimits.push({
            levelNumber: levelData.level,
            levelMaxLimit: levelData.max_value,
            levelMinLimit: levelData.min_value
        })
    });
    var getLineYposition = function ( levelInfo ) {
        return canvas.height_per_level * levelInfo.levelNumber;
    };
    var canvasSelectionEnterCache = canvas.element.selectAll("line").data(levelLimits).enter();
    
    canvasSelectionEnterCache.append("line")
            .attr({
                x1: -canvas.node_radius,
                x2: canvas.width,
                y1: function(d) { return getLineYposition(d); },
                y2: function(d) { return getLineYposition(d); },
                stroke: "#990000",
                "stroke-width": "1px"
            })
      canvasSelectionEnterCache.append("text")
            .text( function ( d ) { 
                return d.levelMinLimit + "-" + d.levelMaxLimit + "k"; 
            })
            .attr({
                "text-anchor": "middle",
                "font-size": "12px",
                y: function(d) { return getLineYposition(d) - canvas.height_per_level / 2; },
                x: canvas.node_radius + 10,
                fill: "#009900"
        });

};

var renderLevel = function ( canvas, levelData ) {
    var getElementY = function ( graphNode ) {
       return ( ( levelData.level - 1 )  * canvas.height_per_level ) + ( canvas.height_per_level / 2 ); 
    };
    var getElementX = function ( graphNode, nodeIndex ) {
        return ( nodeIndex  + 1) * ( canvas.usable_canvas_space / ( levelData.nodes.length + 1 ) );
    };
    var canvasSelectionEnterCache = canvas.element.selectAll().data( levelData.nodes ).enter();
    canvasSelectionEnterCache
        .append("circle")
        .attr({
            cy : function ( d ) { return getElementY( d ); },
            cx : function( d, i ) { return getElementX( d, i ); },
            fill :  function( d ) { return "#ffcc66"; },
            r : canvas.node_radius 
    });
    canvasSelectionEnterCache
        .append("text")
        .text( function ( d ) { return d.company; })
        .attr({
            "text-anchor": "middle",
            "font-size": "12px",
            y: function ( d ) { return getElementY( d ) + canvas.node_radius  / 2 ; },
            x: function( d, i ) { return getElementX( d, i ); },
            class : "companyInfo",
            fill: "#000099"
    });
};

var createDomCache = function() {
    filterValueDomCache = $("#filterValue");
};

startApp();