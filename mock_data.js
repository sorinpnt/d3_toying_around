var mock_data = {
    canvas_info:  {
        height: 300,
        width: 450,
        node_radius: 10,
    },
    graph_data: [{
        level: 1,
        max_value: 600,
        min_value: 500,
        nodes: [{
            id: 1,
            company: 'Company',
            profit: 570
        }]
    },{
        level: 2,
        max_value: 500,
        min_value: 400,
         nodes: [{
            id: 2, 
            company: 'Company2',
            profit: 484
        },{
            id: 3,
            company: 'Company3',
            profit: 420
        },{
            id: 4,
            company: 'Company3',
            profit: 461
        },{
            id: 5,
            company: 'Company3',
            profit: 401
        }]
    },
    {
        level: 3,
        max_value: 400,
        min_value: 300,
         nodes: [{
            id: 6,
            company: 'Company2',
            profit: 390
        },{
            id: 7,
            company: 'Company3',
            profit: 370
        },{
            id: 8,
            company: 'Company4',
            profit: 395
        }]
    },
     {
        level: 4,
        max_value: 300,
        min_value: 200,
         nodes: [{ 
            id: 9,
            company: 'Company2',
            profit: 267
        },{
            id: 10,
            company: 'Company3',
            profit: 231
        },{
            id: 11,
            company: 'Company4',
            profit: 280
        },{
            id: 12,
            company: 'Company2',
            profit: 220
        },{
            id: 13,
            company: 'Company3',
            profit: 247
        },{
            id: 14,
            company: 'Company4',
            profit: 234
        }]
    },
    {
        level: 5,
        max_value: 200,
        min_value: 100,
         nodes: [{
            id: 15,
            company: 'Company2',
            profit: 137
        },{
            id: 16,
            company: 'Company3',
            profit: 150
        },{
            id: 17,
            company: 'Company4',
            profit: 120
        },{ 
            id: 18,
            company: 'Company2',
            profit: 180
        }]
    }]
};
